class MyMiddleware(scrapy.contrib.downloadermiddleware.DownloaderMiddleware):
    def process_request(self,  request, spider):
        if request.url is None:
            raise IgnoreRequest
        
    def  process_exception(self, request, exception, spider):
        pass
    
    def process_response(self):
        pass