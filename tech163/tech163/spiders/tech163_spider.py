from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors import LinkExtractor
from tech163.items import NewsItem
class Tech163_Spider(CrawlSpider):
    name = 'tech163'
    allowed_domains = ["tech.163.com"]
    start_urls = ['http://tech.163.com/']
    
    rules = (
             Rule(LinkExtractor(allow=r"/15/0531/(\d+)/.*"), callback="parse_tech163",follow=True),
        )
        
    def parse_tech163(self, response):
        item = NewsItem()
        item["title"] = response.css('#h1title ::text').extract()[0]
        item['datetime'] = response.css('.ep-time-soure ::text').extract()[0]
        item["src"] = response.css('#ne_article_source ::text').extract()[0].encode('utf-8')
        
        print "src: ",item["src"]
        return item
        
    
    