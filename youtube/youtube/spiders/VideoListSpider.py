import scrapy
from youtube.items import VideoItem

class VideoListSpider(scrapy.Spider):
    name = 'videolist'
    allowed_domains = 'youtube.com'
    #start_urls = ['https://www.youtube.com/watch?v=3CA1Su58SG8&index=1&list=PLQndNexFjUWnRdVNiyFf9u2aO0o1CqS8G']
    start_urls = ['https://www.youtube.com/watch?v=6IV_FYx6MQA&index=16&list=PLirBjzN2aCJlPHizH6KIpzCA5IRDtiB50']
    def parse(self, response):
        links = response.css(".playlist-video ::attr(href)").extract()
        for link in links:
            item = VideoItem()
            item['link'] = 'https://www.youtube.com' + link
            yield item
    