# -*- coding: utf-8 -*-

# Scrapy settings for youtube project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'youtube'

SPIDER_MODULES = ['youtube.spiders']
NEWSPIDER_MODULE = 'youtube.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'youtube (+http://www.yourdomain.com)'
