# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy

class JdGoodItem(scrapy.Item):
    good_name = scrapy.Field()
    good_price = scrapy.Field()
    good_url = scrapy.Field()
    
    
class TutorialItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass
