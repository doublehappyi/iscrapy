import scrapy
from tutorial.items import JdGoodItem

class JdGoodSpider(scrapy.spider.Spider):
    name = "jdgood"
    allowed_domains = "search.jd.com"
    start_urls = ["http://search.jd.com/Search?keyword=%E6%89%8B%E6%9C%BA&enc=utf-8"]
    
    def parse(self, response):
        goods_sels = response.css("ul[class='list-h clearfix'] > li")
        for sel in goods_sels:
            item = JdGoodItem()
            item["good_name"] = sel.css("[class='p-name'] > a ::text").extract()
            item['good_price'] = sel.css("[class='p-price'] > strong ::text").extract()
            item['good_url'] = sel.css("[class='p-name'] > a ::attr('href')").extract()
            yield item